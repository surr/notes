(function(exports){


    var TEMPL_TAG_NAME = 'block-notice_viewer',
        THE_BLOCK_NAME = 'noticeViewer';

    if ( !exports.blocks ) exports.blocks = {};
    if ( exports.blocks.hasOwnProperty(THE_BLOCK_NAME) ) {
        throw('the block ' + THE_BLOCK_NAME + ' already exists');
    }

    exports.blocks[THE_BLOCK_NAME] = Instance;



    /**
     * @param {Object} o
     *      @param {Object} o.container HTML element where to create the block instance
     *      @param {Object} [o.channel] pub/sub insance
     */
    function Instance ( o ) {

        var that = this;

        this.channel = o.channel || new EventManager(true);
        this.container = document.createElement('div');

        this.container.setAttribute('data-role', 'noticeViewer');

        o.container.appendChild(this.container);
    };


    /**
     * @param {Object[]} tagsList
     */
    Instance.prototype.render = function ( notice ) {

        notice = notice || {};

        var _notice = {
            ID      : notice.id || '',
            DATE    : notice.date || '',
            TITLE   : notice.title || '',
            CONTENT : notice.content || '',
            TAGS    : notice.tags
        };
        this.container.style.display = 'block';
        templator.renderTo(TEMPL_TAG_NAME, _notice, this.container);
    };

    Instance.prototype.hide = function () {
        this.container.style.display = 'none';
    };

    Instance.prototype.on = function () {
        return this.channel.subscribe.apply(this.channel, arguments);
    }
})(window);
