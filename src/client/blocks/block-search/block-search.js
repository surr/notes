(function(exports){

    var TEMPL_TAG_NAME = 'block-notice_search',
        THE_BLOCK_NAME = 'search',
        FIRST_ELEM = 0;

    if ( !exports.blocks ) exports.blocks = {};
    if ( exports.blocks.hasOwnProperty(THE_BLOCK_NAME) ) {
        throw('the block ' + THE_BLOCK_NAME + ' already exists');
    }

    exports.blocks[THE_BLOCK_NAME] = Instance;



    /**
     * @param {Object} o
     *      @param {Object} o.container HTML element where to create the block instance
     *      @param {Object} [o.channel] pub/sub insance
     */
    function Instance ( o ) {

        var that = this;

        this.channel = o.channel || new EventManager(true);
        this.container = document.createElement('ul');
        this.checkedTags = {};

        this.container.setAttribute('data-role', 'tag-label');
        this.container.classList.add('list-unstyled')
        o.container.appendChild(this.container);

        this.container.addEventListener('change', function(event){
            var tagName = event.target.dataset.tagName,
                isChecked = event.target.checked;

            that.checkedTags[tagName] = isChecked;

            var checkedTags = that.getChecketTags();

            if ( checkedTags.length === 0 ){

                that._setAllTagsAsActive()
            }

            that.channel.publish(
                'change',
                checkedTags
            );
        });
    }

    Instance.prototype._setAllTagsAsActive = function(){

        var res = $(this.container).find('.tag_name');
        console.log( res );
        res.toggleClass('actualTag', true);
    };

    Instance.prototype.getChecketTags = function(){
        
        var that = this;

        return Object.keys(this.checkedTags)
        .filter(function(tagName){
            return that.checkedTags[tagName];
        })
    };


    /**
     * @param {Object} o
     * @param {String[add|remove]} o.action
     * @param {String[]} o.tags
     */
    Instance.prototype.update = function ( o ) {

        var that = this;

        if ( o.action === 'remove' ) {
            o.tags.forEach(function(tagName){

                if ( that.checkedTags[tagName] ) {
                    delete that.checkedTags[tagName];
                }

                $(that.container)
                .find('li[data-tag-name="'+tagName+'"]')
                .remove();
            });
        } else {
            // here o.action === 'add'
            o.tags.forEach(function(tagName){
                templator.addTo(
                    TEMPL_TAG_NAME,
                    {
                        TAG_NAME : tagName,
                        CHECKED  : that.checkedTags[tagName] ? 'checked' : ''
                    },
                    that.container
                );
            });
        }
    };

    Instance.prototype.on = function () {
        return this.channel.subscribe.apply(this.channel, arguments);
    }

    Instance.prototype.allTagsInNotes = function( list ){

        list = list || [];

        if ( list.length === 0 && this.checkedTags.length === 0 ){

            this._setAllTagsAsActive()
            
        } else {

            $(this.container).find('li[data-tag-name]').each(function(){

                var tagname = $(this).attr('data-tag-name');

                if ( list.indexOf( tagname ) > -1 ){

                    $(this).find('span').toggleClass('actualTag', true);

                }else{

                    $(this).find('span').toggleClass('actualTag', false);
                }
            })
        }
    };

})(window);
