(function(exports){

    var TEMPL_TAG_NAME = 'block-notise_searchResult',
        THE_BLOCK_NAME = 'searchResult';

    if ( !exports.blocks ) exports.blocks = {};
    if ( exports.blocks.hasOwnProperty(THE_BLOCK_NAME) ) {
        throw('the block ' + THE_BLOCK_NAME + ' already exists');
    }

    exports.blocks[THE_BLOCK_NAME] = Instance;



    /**
     * @param {Object} o
     *      @param {Object} o.container HTML element where to create the block instance
     *      @param {Object} [o.channel] pub/sub insance
     */
    function Instance ( o ) {

        var that = this;

        this.channel = o.channel || new EventManager(true);
        this.container = document.createElement('ul');
        this.checkedNoticeId;

        this.container.setAttribute('data-role', 'searchResultItem-label');
        this.container.classList.add('list-unstyled')

        o.container.appendChild(this.container);

        this.container.addEventListener('change', function(event){

            that.checkedNoticeId = event.target.dataset.noticeId;

            that.channel.publish('change', that.checkedNoticeId);
        });
    };


    /**
     * @param {Object[]} tagsList
     */
    Instance.prototype.update = function ( searchResults ) {

        var that = this,
            isCheckedActual = false;

        searchResults = searchResults || [];

        this.container.innerHTML = '';

        searchResults.forEach(function(notice){
            if ( notice.id === that.checkedNoticeId ) isCheckedActual = true;
            
            templator.addTo(
                TEMPL_TAG_NAME,
                {
                    NOTICE_ID : notice.id,
                    TITLE     : notice.title,
                    CHECKED   : that.checkedNoticeId === notice.id,
                    TAGS_LIST   : notice.tags,
                    NOTICE_DATE : notice.date        
                },
                that.container
            );
        });

        if ( !isCheckedActual ) {
            that.checkedNoticeId = undefined;
            that.channel.publish('change', that.checkedNoticeId);
        }
    };

    Instance.prototype.on = function () {
        return this.channel.subscribe.apply(this.channel, arguments);
    }

})(window);
