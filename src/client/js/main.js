(function(){
    var blocksBin = {},
        containers = {
            search       : document.querySelector('div[data-role="search"]'),
            searchResult : document.querySelector('div[data-role="searchResult"]'),
            noticeCont   : document.querySelector('div[data-role="noticeCont"]')
        },
        addNoticeButton = document.querySelector('div[data-role="cont"] button');


    blocksBin.searchResult = new blocks.searchResult({
        container : containers.searchResult
    });
    
    blocksBin.search = new blocks.search({
        container : containers.search
    });
    
    blocksBin.noticeEditor = new blocks.noticeEditor({
        container : containers.noticeCont
    });
    
    blocksBin.noticeViewer = new blocks.noticeViewer({
        container : containers.noticeCont
    });
    
    addNoticeButton.addEventListener('click', function(){
        blocksBin.noticeViewer.hide();
        blocksBin.noticeEditor.render();
    });
    
    
    blocksBin.noticeEditor.on('save', function(event, notice){
        indexedStorage.create(notice, function(error){
            if ( error ) return console.error(error);
        });
    });
    
    blocksBin.search.on('change', function(event, checkedTags){
        checkedTags = checkedTags || [];
    
        if ( !checkedTags.length ){
            blocksBin.searchResult.update([]);
            blocksBin.search.allTagsInNotes()////// ?
            return 
        }
    
        indexedStorage.findByTags(checkedTags, function(error,response){
            if ( error ) throw(error);
    
            blocksBin.searchResult.update(response.notes)
            blocksBin.search.allTagsInNotes(response.availableTags)//// ?
        });
    });
    
    blocksBin.searchResult.on('change', function(event, checkedNoticeId){

        if ( !checkedNoticeId ) {
            blocksBin.noticeEditor.hide();
            blocksBin.noticeViewer.hide();
            return;
        }

        indexedStorage.get(checkedNoticeId, function(error, notice){
            blocksBin.noticeEditor.hide();
            blocksBin.noticeViewer.render(notice);
        })
    });
    
    
    indexedStorage.on('newtags', function(event, list){
        blocksBin.search.update({
            action : 'add',
            tags   : list
        });
    });
    
    indexedStorage.on('deletedtags', function(event, list){
        blocksBin.search.update({
            action : 'remove',
            tags   : list
        });
    });
    
    indexedStorage.tagsList(function(error, list){
        blocksBin.search.update({
            action : 'add',
            tags   : list
        });
    });
})();
