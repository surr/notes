var indexedStorage = (function(){

    var events = new EventManager;

    return {

        get : function ( id, callback ) {
            $.ajax({url:'/_/api/v1/notes/' + id, type:'get'})
            .done(function(res){callback(null, res.note)})
            .fail(function(error){callback(error)})
        },

        create : function ( o, callback ) {
            $.ajax({url:'/_/api/v1/notes', type:'post', data : {json : JSON.stringify(o)}})
            .done(function(res){
                callback(null);

                //events.publish('tagschange', __index.list());
                events.publish('newtags', res.newTags);
            })
            .fail(function(error){callback(error)})
        },

        remove : function ( id, callback ) {
            $.ajax({url:'/_/api/v1/notes/' + id, type:'delete'})
            .done(function(res){
                callback(null);

                events.publish('deletedtags', res.deletedTags);
            })
            .fail(function(error){callback(error)})
        },
    
        findByTags : function ( tags, callback ) {
            $.ajax({url:'/_/api/v1/notes', type:'get', data : {tags : tags}})
            .done(function(response){
                callback(null, response)
            })
            .fail(function(error){callback(error)})

        },

        tagsList : function (callback) {
            $.ajax({url:'/_/api/v1/tags', type:'get'})
            .done(function(res){
                callback(null, res.list)
            })
            .fail(function(error){callback(error)})
        },

        on : events.subscribe.bind(events)

    };



})();
