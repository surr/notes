(function(exports){
    
    exports.templator = {

        addTo : function ( tmplName, data, target ) {

            return render( tmplName, data, target )
            .then( function( html ){

                target.innerHTML += html; 
                
            })
        },

        renderTo : function ( tmplName, data, target ) {
            
            return render( tmplName, data, target )
            .then( function( html ){

                target.innerHTML = html; 
                
            })
        }
    };

    function render ( tmplName, data ) {

        return $.get('/_/templates/'+ tmplName +'.jade')
        .then(function(template){
            var render = jade.compile(template),
            html = render( data );

            return html;
        })
    }
})(window);