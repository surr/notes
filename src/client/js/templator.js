(function(exports){
    
    exports.templator = {

        addTo : function ( tmplName, data, target ) {
            target.innerHTML += render( tmplName, data, target );
        },

        renderTo : function ( tmplName, data, target ) {
            target.innerHTML = render( tmplName, data, target );
        }
    };

    function render ( tmplName, data, target ) {
        var tmpl = document.querySelector('[data-template-name="'+tmplName+'"]')
                    .innerHTML.split('%%');

        return tmpl.reduce(function(p, c, i, a){
            if ( i%2 === 0 ) return p + c;
    
            return p + data[c];
        }, '');
    }

})(window);
