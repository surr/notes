var Q    = require('q'),

    convert10to62 = require('./utils/convert10to62'),

    db,
    DB_PREFIX,
    NOTES_CL,
    INFO_CL,
    TAGS_CL;


var notes = {
    get : function ( req, res, next ) {
        var userID = req.user.login,
            noteID = req.params.id;

        if ( !noteID ) return res.status(400).json({
            status : 'failed',
            entity : 'note',
            action : 'get',
            noteID : noteID,
            userID : userID,
            error : {
                code : 1,
                description : 'note id is required for [GET|DELETE] notes/:id method'
            }
        });

        db.get(getDbName(userID), NOTES_CL, {id : noteID})
        .then(function(docs){
            if ( !docs || !docs.length ) return res.status(404).json({
                status : 'failed',
                entity : 'note',
                action : 'get',
                noteID : noteID,
                userID : userID,
                error : {
                    code : 2,
                    description : 'can not find the note'
                }
            });

            return res.json({
                status : 'success',
                entity : 'note',
                action : 'get',
                noteID : noteID,
                userID : userID,
                note   : docs[0]
            });
        })
        .fail(function(error){

            console.log('ERROR [API.GET] : ', error);

            return res.status(500).json({
                status : 'failed',
                entity : 'note',
                action : 'get',
                noteID : noteID,
                userID : userID,
                error : {
                    code : 3,
                    description : 'server error'
                }
            });
        })
    },

    rm : function ( req, res, next ) {
        var userID = req.user.login,
            noteID = req.params.id,
            deletedTags = [];

        if ( !noteID ) return res.status(400).json({
            status : 'failed',
            entity : 'note',
            action : 'delete',
            noteID : noteID,
            userID : userID,
            error : {
                code : 1,
                description : 'note id is required for [GET|DELETE] notes/:id method'
            }
        });

        db.rm(getDbName(userID), NOTES_CL, {id : noteID})
        .then(function(){
            return db.update(
                getDbName(userID),
                    TAGS_CL,
                    {name : { $exists :true } },
                    // TODO update when removing multi items
                    { $pullAll : { notes : [noteID] } }
            )
            .then(function(){
                return db.get(
                    getDbName(userID),
                    TAGS_CL,
                    {notes : { $size : 0 } }
                )
                .then(function(empty){
                    deletedTags = empty.map(function(tag){ return tag.name });

                    return db.rm(
                        getDbName(userID),
                        TAGS_CL,
                        {notes : { $size : 0 } }
                    )
                })
            })
        })
        .then(function(){
            return res.json({
                status      : 'success',
                entity      : 'note',
                action      : 'delete',
                noteID      : noteID,
                userID      : userID,
                deletedTags : deletedTags
            });
        })
        .fail(function(error){

            console.log('ERROR [API.GET] : ', error);

            return res.status(500).json({
                status : 'failed',
                entity : 'note',
                action : 'delete',
                noteID : noteID,
                userID : userID,
                error : {
                    code : 3,
                    description : 'server error'
                }
            });
        })
    },
    create : function ( req, res, next ) {
        var userID  = req.user.login,
            json    = req.body.json, // TODO
            newTags = [],
            note;

        try { note = JSON.parse(json) }
        catch (error) {
            return res.status(400).json({
                status : 'failed',
                entity : 'note',
                action : 'create',
                json   : json,
                userID : userID,
                error : {
                    code : 4,
                    description : 'can not parse the json',
                    message : error.message
                }
            });
        }

        db.update(
            getDbName(userID),
            INFO_CL,
            { name : 'notesQuantity' },
            { $inc : { value : 1 } },
            {
                upsert         : true,
                multi          : false,
                returnOriginal : false,
                projection     : { value : 1 }
            }
        )
        .then(function(doc){
            note.id   = convert10to62(doc.value.value);
            note.date = new Date();

            return db.put(getDbName(userID), NOTES_CL, note)
            .then(function(){
                return db.get(
                    getDbName(userID),
                    TAGS_CL,
                    { name : { $in : note.tags } }
                )
            })
            .then(function(tagsDocs){

                var existingTags = tagsDocs.map(function(doc){ return doc.name });

                note.tags.forEach(function(tag){
                    if ( !~existingTags.indexOf(tag) ) newTags.push(tag);
                });

                return Q.all([
                    db.update(
                        getDbName(userID),
                        TAGS_CL,
                        { name : { $in : existingTags } },
                        { $addToSet : { notes : note.id } },
                        { upsert : false }
                    ),
                    (
                        newTags.length
                            ? db.put(
                                    getDbName(userID),
                                    TAGS_CL,
                                    newTags.map(function(tag){
                                        return {name:tag, notes:[note.id]}
                                    })
                                )
                            : Q.resolve()
                    )
                ]);
            })
        })
        .then(function(){
            return res.json({
                status  : 'success',
                entity : 'note',
                action  : 'create',
                noteID  : note.id,
                userID  : userID,
                newTags : newTags
            });
        })
        .fail(function(error){

            console.log('ERROR [API.CREATE] : ', error);

            return res.status(500).json({
                status : 'failed',
                entity : 'note',
                action : 'create',
                noteID : note.id,
                userID : userID,
                error : {
                    code : 3,
                    description : 'server error'
                }
            });
        });
    },
    search : function ( req, res, next ) {
        var userID = req.user.login,
            tags   = req.query.tags,
            ids    = req.query.ids,
            fields = req.query.fields || [];

        // default returned fields for a note
        if ( fields.indexOf('tags')  === -1 ) fields.push('tags');
        if ( fields.indexOf('title') === -1 ) fields.push('title');
        if ( fields.indexOf('id')    === -1 ) fields.push('id');
        if ( fields.indexOf('date')  === -1 ) fields.push('date');

        (function(){
            return !ids && ( tags && tags.length )
                ? db.get(getDbName(userID), NOTES_CL, { tags : { $all : tags } })
                : !tags ( ids && ids.length )
                    ? db.get(getDbName(userID), NOTES_CL, { id : { $in : ids } })
                    : res.status(400).json({
                            status : 'failed',
                            entity : 'note',
                            action : 'search',
                            ids    : ids,
                            tags   : tags,
                            userID : userID,
                            error : {
                                code : 6,
                                description : 'either "tags" or "ids" ' +
                                    'must be specified and to be not empty array'
                            }
                        });
        })()
        .then(function(docs){

            var availableTags = {},
                    cuttedNotes = docs.map(function(fullNote){
                    var note = {};

                    fields.forEach(function(required){
                        note[required] = fullNote[required];
                    });

                    fullNote.tags.forEach(function(tag){
                        availableTags[tag] = true;
                    });

                    return note;
                });

            return res.json({
                status        : 'success',
                entity        : 'note',
                action        : 'search',
                ids           : ids,
                tags          : tags,
                userID        : userID,
                notes         : cuttedNotes,
                availableTags : Object.keys(availableTags)
            });
        })
        .fail(function(error){

            console.log('ERROR [API.GET] : ', error);

            return res.status(500).json({
                status : 'failed',
                entity : 'note',
                action : 'search',
                ids    : ids,
                tags   : tags,
                userID : userID,
                error : {
                    code : 3,
                    description : 'server error'
                }
            });
        })
    }
};


var tags = {
    get : function ( req, res, next ) {
        var userID = req.user.login;

        return db.get(getDbName(userID), TAGS_CL)
        .then(function(docs){
            var tags = docs.map(function(tag){ return tag.name });

            return res.json({
                status : 'success',
                entity : 'tag',
                action : 'list',
                list   : tags,
                userID : userID
            });
        });
    }
};

exports.init = function ( o ) {
    db        = o.db;
    DB_PREFIX = o.DB_PREFIX;
    NOTES_CL  = o.NOTES_CL;
    TAGS_CL   = o.TAGS_CL;
    INFO_CL   = o.INFO_CL;

    delete exports.init;

    return {
        notes : notes,
        tags  : tags
    };
};

function getDbName ( login ) {
    return DB_PREFIX + login
};


