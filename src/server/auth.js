// primitive auth
// TODO normal auth

var uuid = require('node-uuid'),
    Q    = require('q'),

    auth;

function Constr ( o ) {
    if ( !(this instanceof Constr) ) return new Constr(o);
    if ( auth ) return auth;

    this._user = o.user;
    this._db   = o.sessionStorage;
    this._cookieName = o.cookieName;
    this._loginScreenURL = o.loginScreenUrl;
    this._doNotRedirectToLoginScreenIf = o.doNotRedirectToLoginScreenIf || [];

    auth = this;
}

Constr.prototype.loginScreen = function ( req, res, next ) {

    (
        req.cookies[auth._cookieName]
            ?  auth._db.rm(req.cookies[auth._cookieName])
            : Q.resolve()
    )
    .then(function(){

        res.clearCookie(auth._cookieName, {
            path : this.path
        });

        res.render('loginScreen');
    })
    .done();
};

Constr.prototype.login = function ( req, res, next ) {
    var params = req.body;

    auth._user.get({login : params.login})
    .then(function(user){
        if ( !user || user.pass !== params.password ) return res.redirect(auth._loginScreenURL);

        var sessionId = uuid.v1();

        return auth._db.put({
            id      : sessionId,
            created : new Date,
            user : {
                    login : params.login
                },
            data : {}
        })
        .then(function(){
            res.cookie(auth._cookieName, sessionId, {
                path    : '/',
                //expires : new Date(session.expires)
            });

            res.redirect('/');
        })
    })
    .done();
};

Constr.prototype.sessionGate = function ( req, res, next ) {

    if ( req.path === auth._loginScreenURL ) return next();

    var sessionId = req.cookies[auth._cookieName];

    if ( !sessionId ) {
        var doRedirect = true,
            i = 0,
            l = auth._doNotRedirectToLoginScreenIf.length;

        res.status(401);

        while ( doRedirect && i < l ) {
            if ( auth._doNotRedirectToLoginScreenIf[i] instanceof RegExp ) {
                if ( auth._doNotRedirectToLoginScreenIf[i].test(req.path) ) {
                    doRedirect = false;
                }
            }
            i++;
        }

        return doRedirect
            ? res.redirect(auth._loginScreenURL)
            : res.end();
    }

    auth._db.get(sessionId)
    .then(function(session){

        req.session = session.data;
        req.user    = session.user;

        var _end = res.end,
            oldSession = JSON.stringify(session.data);

        res.end = function () {
            var newSession = JSON.stringify(req.session);

            if ( oldSession !== newSession ) {
                auth._db.update(sessionId, {data : req.session}).done();
            }

            _end.apply(res, arguments);
        };

        next();
    })
    .fail(function(error){
        if ( error ) console.log('ERROR [AUTH.GATE] : ', error);
        res.redirect(auth._loginScreenURL);
    })
};

module.exports = Constr;
