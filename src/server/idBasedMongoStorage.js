var Q = require('q');


function Constructor ( o ) {
    this.database   = o.database;
    this.collection = o.collection;
    this.keyField   = o.keyField;
    this.db = o.db;
};

Constructor.fn = Constructor.prototype;

Constructor.fn.get = function ( id ) {

    if ( !id ) return Q.reject('need document id');

    var query = {};
    query[this.keyField] = id;

    return this.db.get(this.database, this.collection, query)
    .then(function ( docs ){
        return docs && docs[0] || Q.reject(Error('can not get ' + id));
    });
};


Constructor.fn.put = function ( o ) {

    if ( typeof o !== 'object' ) return Q.reject(Error('object is expected'));

    return this.db.put(this.database, this.collection, o);
};


/**
 * @param {String|Number|Object} id id or mongodb query
 * @returns {Promise}
 */
Constructor.fn.rm = function ( id ) {

    var query = {},
        type  = typeof id;

    query[this.keyField] = id;

    if ( type !== 'string' && type !== 'number' ) {
        return Q.reject(Error('Wrong type of argument: ' + type));
    }

    return this.db.rm(this.database, this.collection, query)
};


Constructor.fn.update = function ( id, fields ) {

    if ( !id ) return Q.reject('need document id');

    if ( typeof fields !== 'object' ) return Q.reject(Error('object is expected'));

    var query = {},
        type  = typeof id;

    query[this.keyField] = id;

    if ( type !== 'string' && type !== 'number' ) {
        return Q.reject(Error('Wrong type of argument: ' + type));
    }

    return this.db.update(
        this.database,
        this.collection,
        query,
        {$set : fields},
        {multi: false}
    );
};


module.exports = Constructor;
