// TODO check multi-put
var Q = require('q'),
    MDB = require('mongodb'),
    MCL = require('mongodb').MongoClient,
    OLDVERSION = !!( MDB.BSONPure && MDB.BSONPure.ObjectID ),
    ObjectID = OLDVERSION ? MDB.BSONPure.ObjectID : MDB.ObjectID;

var cache = {},
    DEFAULTS = {
        maxOpenedConnections: 200,
        host: 'localhost',
        port: 27017
    };

/**
 * @constructor
 * @param {Object} options
 *
 * @returns {Object}
 */
function Mdbw(options) {
    if (!(this instanceof Mdbw)) return new Mdbw(options);
    options = options || {};

    if (options.user && options.pass) {
        this.creds = {
            user: options.user,
            pass: options.pass
        }
    }

    var hostPort = this._hostPort = [
        'mongodb://',
            options.host || DEFAULTS.host,
        ':',
            options.port || DEFAULTS.port,
        '/'
    ].join('');

    if (cache[hostPort]) return this;

    var D = Q.defer();
    cache[hostPort] = {
        options: {
            maxOpenedConnections: options.maxOpenedConnections - 2
                // why ( -2 ) :
                // +1 root,  +1 next connection ( it checks before connecting to new )
                || DEFAULTS.maxOpenedConnections
        },
        connections: {},
        queue : [],
        handlers : {},
        root: {},
        ready: D.promise
    };

    var that = this;
    connect(hostPort, '')
        .then(function (db) {
            if (!that.creds) {
                cache[hostPort].root = db;
                D.resolve();
                return;
            }
            db.admin().authenticate(
                that.creds.user,
                that.creds.pass,
                function (error, result) {
                    if (error) console.log('MDBW.ERROR: root auth failed');

                    cache[hostPort].root = db;
                    D.resolve();
                }
            );
        })
        .fail(function(e){
            console.log("Can't connect to MongoDB",e);
        })
        .done();
}

// for tests only
Mdbw.prototype._cache = cache;

/**
 * @param {String} dbName database name
 * @param {String} clName collection name
 * @param {Object} query mongodb query
 * @param {Object} sort sort order
 *
 * @returns {Promise | Array} array of strings (names) if was not specified query,
 *                  and array of objects (documents) otherwise
 */
Mdbw.prototype.get = function (dbName, clName, query, sort) {
    var hostPort = this._hostPort;
    return smartConnect({hp: hostPort, db: dbName, cl: clName, creds: this.creds})
        .then(function (cnct) {
            if (cnct === null) return [];
            if (!dbName) return getDatabasesList(cnct);
            if (!clName) return getCollectionsNames(cnct);
            return getDocuments(cnct, query, sort);
        })
        .then(function(result){
            return reviewConnectionsAndReturn(hostPort, dbName, false, result);
        })
};

/**
 * @param {String} dbName database name
 * @param {String} clName collection name
 * @param {Object} query mongodb query
 * @param {Object} fields mongodb projection
 * @param {Object} sort sort order
 *
 * @returns {Promise | Array} array of strings (names) if was not specified query,
 *                  and array of objects (documents) otherwise
 */
Mdbw.prototype.select = function (dbName, clName, query, fields, sort) {
    var hostPort = this._hostPort;
    return smartConnect({hp: hostPort, db: dbName, cl: clName, creds: this.creds})
        .then(function (cnct) {
            if (cnct === null) return [];
            if (!dbName) return getDatabasesList(cnct);
            if (!clName) return getCollectionsNames(cnct);
            return selectDocuments(cnct, query, fields, sort);
        })
        .then(function(result){
            return reviewConnectionsAndReturn(hostPort, dbName, false, result);
        })
};

/**
 * @param {String} dbName database name
 * @param {String} clName collection name
 * @param {Object} doc document
 */
Mdbw.prototype.put = function (dbName, clName, doc) {
    if (!dbName) return Q.reject('need database name at least. nothing to put');
    var hostPort = this._hostPort;
    return smartConnect({hp: hostPort, db: dbName, fc: true, creds: this.creds})
        .then(function (db) {
            if (!clName) return getCollectionsNames(db).then(function () {
            });
            if (!doc) return createCollection(db, clName);
            return saveDocument(db, clName, doc);
        })
        .then(function(result){
            return reviewConnectionsAndReturn(hostPort, dbName, false, result);
        })
}

/**
 * @param {String} dbName database name
 * @param {String} clName collection name
 * @param {Object} query mongodb query
 *
 * @returns {Promise | Boolean}
 */
Mdbw.prototype.exists = function (dbName, clName, query) {
    return this.count(dbName, clName, query)
        .then(function (n) {
            return !!n
        });
}

/**
 * @param {String} dbName database name
 * @param {String} clName collection name
 * @param {Object} query mongodb query
 * @param {Object} fields what to update
 * @param {Object} [o={}] options
 *
 * @returns {Promise | Number} quantity of updated documents
 */
Mdbw.prototype.update = function (dbName, clName, query, fields, o) {
    if (arguments.length < 4) return Q.reject('need four or five arguments');
    o = o || {};
    var hostPort = this._hostPort,
        clearOptions = o || {};

    clearOptions.multi  = typeof o.multi  === 'boolean' ? o.multi  : true;
    clearOptions.upsert = typeof o.upsert === 'boolean' ? o.upsert : false;

    return smartConnect({
        hp    : hostPort,
        db    : dbName,
        cl    : clName,
        creds : this.creds,
        fc    : clearOptions.upsert
    })
    .then(function (cl) {
        if (cl === null) return 0;
        return o.hasOwnProperty('projection') || o.hasOwnProperty('returnOriginal')
            ? updateAndReturn(cl, query, fields, clearOptions)
            : update(cl, query, fields, clearOptions);
    })
    .then(function(result){
        return reviewConnectionsAndReturn(hostPort, dbName, false, result);
    })
};

/**
 * @param {String} dbName database name
 * @param {String} clName collection name
 * @param {Object} query mongodb query
 *
 * @returns {Promise | Number} quantity of removed items
 */
Mdbw.prototype.rm = function (dbName, clName, query) {
    if (!dbName) return Q.reject('need at least name of a database');
    var hostPort = this._hostPort,
        rm = false;
    return smartConnect({hp: hostPort, db: dbName, cl: clName, creds: this.creds})
        .then(function (cnct) {
            if (cnct === null) return 0;
            if (!clName) {
                rm = true;
                return removeDatabase(cnct)
            }
            if (!query) return removeCollection(cnct)
            return removeDocuments(cnct, query);
        })
        .then(function(result){
            return reviewConnectionsAndReturn(hostPort, dbName, rm, result);
        })
};

/**
 * @param {String} dbName database name
 * @param {String} clName collection name
 *
 * @returns {Promise | Object} node-mongodb-native objects
 *              for database, or collection
 */
Mdbw.prototype.native = function (dbName, clName) {
    var hostPort = this._hostPort;
    return smartConnect({hp: hostPort, db: dbName, cl: clName, creds: this.creds})
    .then(function(db){
        var _close = db.close;

        db.close = function () {
            db.close = _close;
            return reviewConnectionsAndReturn(hostPort, dbName, true);
        };

        return db;
    })
};

/**
 * @param {String} dbName database name
 * @param {String} clName collection name
 * @param {Object} query mongodb query
 *
 * @returns {Promise | Object}
 */
Mdbw.prototype.getOne = function (dbName, clName, query) {
    if (arguments.length != 3) return Q.reject('need exactly three arguments');
    var hostPort = this._hostPort;
    return smartConnect({hp: hostPort, db: dbName, cl: clName, creds: this.creds})
        .then(function (cl) {
            if (cl === null) return undefined;
            return findOne(cl, query)
        })
        .then(function(result){
            return reviewConnectionsAndReturn(hostPort, dbName, false, result);
        })
};

/**
 * @param {String} dbName database name
 * @param {String} clName collection name
 * @param {Object} query mongodb query
 *
 * @returns {Promise | Number}
 */
Mdbw.prototype.count = function (dbName, clName, query) {
    var hostPort = this._hostPort;
    return smartConnect({hp: hostPort, db: dbName, cl: clName, creds: this.creds})
        .then(function (cnct) {
            if (cnct === null) return 0;
            if (!dbName) return getDatabasesList(cnct).then(function (dbs) {
                return dbs.length
            });
            if (!clName) return getCollectionsNames(cnct).then(function (cls) {
                return cls.length
            });
            return count(cnct, query);
        })
        .then(function(result){
            return reviewConnectionsAndReturn(hostPort, dbName, false, result);
        })
};

/**
 * closes all opened connections
 */
Mdbw.prototype.exit = function () {
    var that = this;
    var promises = [];
    var connections = cache[this._hostPort].connections;
    for (var dbName in connections) {
        //console.log('======== exiting ' + dbName);
        promises.push(closeConnection(that._hostPort, dbName));
    }
    return Q.all(promises).then(function(){
        //console.log('======== exiting ROOT');
        //try {
            cache[that._hostPort].root.close(); // TODO imposible to open after exit
        //} catch ( e ) {
        //    console.log('ERROR closing ROOT', e);
        //}
        //console.log( JSON.stringify(connections, null, 4) );
        return Q.resolve();
    });
};

/**
 * @param {Object} db mongodb's db
 * @param {String} clName collection name
 * @param {Object} query mongodb's query
 *
 * @returns {Promise | Number} quantity of removed documents
 */
function removeDocuments(cl, query) {
    var D = Q.defer();
    if (query['_id']) {
        query['_id'] = typeof query['_id'] !== 'object'
            ? ObjectID(query['_id'])
            : query['_id'];
    }
    cl.remove(query, function (error, quantity) {
        return error
            ? D.reject(error)
            : D.resolve(quantity);
    })
    return D.promise;
}

/**
 * @param {Object} db mongodb's db
 * @param {String} clName collection name
 *
 * @returns {Promise | Number} quantity of removed collections
 */
function removeCollection(cl) {
    var D = Q.defer();
    cl.drop(function (error, result) {
        return error ? D.reject(error) : D.resolve(result ? 1 : 0);
    });
    return D.promise;
}

/**
 * @param {Object} db mongodb's db
 *
 * @returns {Promise | Number} quantity of removed databases
 */
function removeDatabase(db) {
    var D = Q.defer();
    db.dropDatabase(function (error) {
        return error ? D.reject(error) : D.resolve(1);
    });
    return D.promise;
}

/**
 * @param {Object} cl mongodb's cursor of collection
 * @param {Object} query mongodb's query for searching documents
 * @param {Object} fields mongodb's query for updating
 *
 * @returns {Promise | Number} quantity of updating documents
 */
function update(cl, _query, fields, options) {
    var D = Q.defer(),
        query = clone(_query);
    if (query && query._id && typeof query._id === 'string') {
        query._id = new ObjectID.createFromHexString(query._id);
    }
    cl.update(query, fields, options, function (error, result) {

        return error
            ? D.reject(error)
            : OLDVERSION
                ? D.resolve(result)
                : D.resolve(result.result.n)
    });
    return D.promise;
}

/**
 * @param {Object} cl mongodb's cursor of collection
 * @param {Object} query mongodb's query for searching document
 * @param {Object} fields mongodb's query for updating
 *
 * @returns {Promise | Number} projection of the updated document
 */
function updateAndReturn(cl, _query, fields, options) {
    var D = Q.defer(),
        query = clone(_query);
    if (query && query._id && typeof query._id === 'string') {
        query._id = new ObjectID.createFromHexString(query._id);
    }
    cl.findOneAndUpdate(query, fields, options, function (error, result) {

        return error
            ? D.reject(error)
            : OLDVERSION
                ? D.resolve(result)
                : D.resolve(result)
    });
    return D.promise;
}

/**
 * @param {Object} cl mongodb's cursor of collection
 * @param {Object} query mongodb's query
 *
 * @returns {Promise | Object} document
 */
function findOne(cl, query) {
    var D = Q.defer();
    cl.findOne(query, function (error, doc) {
        return error ? D.reject(error) : D.resolve(doc || undefined);
    });
    return D.promise;
}

/**
 * @param {Object} cl mongodb's cursor of collection
 * @param {Object} query mongodb's query
 *
 * @returns {Promise | Number} quantity of document matching the query
 */
function count(cl, query) {
    var D = Q.defer();
    cl.count(query, function (error, count) {
        return error ? D.reject(error) : D.resolve(count || 0);
    });
    return D.promise;
}

/**
 * @param {Object} db mongodb's db
 * @param {String} clName collection name
 * @param {Object} doc document
 *
 * @returns {Promise}
 */
function saveDocument(db, clName, doc) {
    if (typeof doc !== 'object') return Q.reject('document should be an object');
    return connectCollection(db, clName)
        .then(function (cl) {
            var D = Q.defer();
            if (doc['_id']) {
                doc['_id'] = typeof doc['_id'] !== 'object'
                    ? ObjectID(doc['_id'])
                    : doc['_id'];
                cl.save(doc, function (error, result) {
                    return error ? D.reject(error) : D.resolve();
                });
            } else {
                if ( OLDVERSION ) {
                    cl.insert(doc, function (error, result) {
                        return error
                            ? D.reject(error)
                            : D.resolve(result[0]['_id'].toHexString());
                    });
                } else {
                    var method = doc instanceof Array ? 'insertMany' : 'insertOne';

                    cl[method](doc, function (error, result) {
                        return error
                            ? D.reject(error)
                            : D.resolve(result.insertedId)
                    });
                }
            }
            return D.promise;
        })
}

/**
 * @param {Object} db mongodb's db
 * @param {String} clName collection name
 *
 * @returns {Promise}
 */
function createCollection(db, clName) {
    var D = Q.defer();
    db.createCollection(clName, function (error, collection) {
        return error
            ? D.reject(error)
            : D.resolve();
    });
    return D.promise;
}

/**
 * @param {Object} db
 *
 * @returns {Promise | Array}
 */
function getDatabasesList(db) {
    var D = Q.defer();
    db.admin().listDatabases(function (error, dbs) {
        if (error) return D.reject(error);
        var list = dbs.databases.map(function (c) {
            return c.name
        });
        return D.resolve(list);
    });
    return D.promise;
}

/**
 * @param {Object} collection
 * @param {Object} _query
 * @param {Object} sort The field or fields to sort by and a value of 1 or -1
 *   to specify an ascending or descending sort respectively
 *
 * @returns {Promise | Array} array of objects
 */
function getDocuments(collection, _query, sort) {
    var D = Q.defer(),
        query = clone(_query);
    if (query && query._id && typeof query._id === 'string') {
        query._id = new ObjectID.createFromHexString(query._id);
    }
    var cursor = collection.find(query);
    if (sort && typeof sort == 'object') {
        cursor.sort(sort);
    }
    cursor.toArray(function (error, arr) {
        return error ? D.reject(error) : D.resolve(arr);
    });
    return D.promise;
}

/**
 * @param {Object} collection
 * @param {Object} _query
 * @param {Object} projection If the argument is specified, the matching documents contain only
 *   the projection fields and the _id field. You can optionally exclude the _id field.
 * @param {Object} sort The field or fields to sort by and a value of 1 or -1
 *   to specify an ascending or descending sort respectively
 *
 * @returns {Promise | Array} array of objects
 */
function selectDocuments(collection, _query, projection, sort) {
    var D = Q.defer(),
        query = clone(_query);
    if (query && query._id && typeof query._id === 'string') {
        query._id = new ObjectID.createFromHexString(query._id);
    }
    var cursor = collection.find(query, projection);
    if (sort && typeof sort == 'object') {
        cursor.sort(sort);
    }
    cursor.toArray(function (error, arr) {
        return error ? D.reject(error) : D.resolve(arr);
    });
    return D.promise;
}


/**
 * @param {Object} db mongodb's db
 * @param {String} clName collection name
 *
 * @returns {Promise | Object} mongodb's cursor
 */
function connectCollection(db, clName) {
    var D = Q.defer();
    db.collection(clName, function (error, collection) {
        return error ? D.reject(error) : D.resolve(collection);
    });
    return D.promise;
}

/**
 * @param {Object} db
 *
 * @returns {Promise | Array} array of strings
 */
function getCollectionsNames(db) {
    var D = Q.defer();
    if ( OLDVERSION ) {
        db.collectionNames(_getCollectionsHandler.bind(null, D))
    } else {
        db.listCollections().toArray(_getCollectionsHandler.bind(null, D));
    }
    return D.promise;
}


function _getCollectionsHandler (D, error, collections) {
    if (!collections) return D.resolve([]);
    var collectionsArr = collections.filter(function (e) {
        return !/\.system\.indexes$/.test(e.name);
    }).map(function (c) {
        return c.name.replace(/^.+\.([^.]+)$/, '$1');
    });
    return error ? D.reject(error) : D.resolve(collectionsArr);
}

/**
 * looking for link to the database in cache first,
 * then connects, if not found
 *
 * @param {Object} o
 * @param {String} o.db database name
 * @param {String} o.cl collection name
 * @param {String} o.hp host + port ex: mongobd://localhost:27017/
 * @param {Boolean} o.fc force ( to connect if a database is not exists )
 *
 * @returns {Promise | Object} mongodb's 'db' or 'cl'
 *          or null if 'force' is not true and database or collection is not exists
 */
function smartConnect(o) {
    var dbName = o.db || '',
        clName = o.cl || '',
        hostPort = o.hp,
        force = o.fc || false;

    if (clName && !dbName) return Q.reject('can not read collection of nothing');

    return cache[hostPort].ready.then(function () {
        var _db = dbName
            ? cache[hostPort].connections[dbName]
            : cache[hostPort].root;

        if ( dbName ) cache[hostPort].handlers[dbName] = ( cache[hostPort].handlers[dbName] || 0 ) + 1;

        if (_db) {

            if (!clName) return _db; // TODO (now it either promise or db)

            return _db.then(function(db){
                return getCollectionsNames(db)
                .then(function (cls) {
                    return !~cls.indexOf(clName) && !force ? null : connectCollection(db, clName);
                })
            });
        }

        return getDatabasesList(cache[hostPort].root)
            .then(function (dbs) {
                if (dbs.indexOf(dbName) === -1 && !force) return null;

                return connectAndWrite(hostPort, dbName)
                .then(function (db) {
                    if (!o.creds) return db;

                    var D = Q.defer();
                    db.admin().authenticate(
                        o.creds.user,
                        o.creds.pass,
                        function (error, result) {
                            if (error) console.log('MDBW.ERROR: auth failed');
                            D.resolve(db);
                        }
                    );
                    return D.promise;
                })
                .then(function (db) {
                    if (!clName) return db;
                    return getCollectionsNames(db)
                        .then(function (cls) {
                            return !~cls.indexOf(clName) && !force
                                ? null
                                : connectCollection(db, clName);
                        })
                })
            })
    });
}

/**
 * connects to the database and writes the link to cache
 *
 * @param {String} hostPort something like this: mongobd://localhost:27017/
 * @param {Object} dbName database name
 *
 * @returns {Promise | Object} mongodb's db
 */
function connectAndWrite(hostPort, dbName) {
    if ( isFreeSlots(hostPort) ) {
        return cache[hostPort].connections[dbName] = connect(hostPort, dbName);
    } else {
        return addToQueue(hostPort, dbName)
    }
}

function isFreeSlots ( hostPort ) {
    return Object.keys(cache[hostPort].connections).length < cache[hostPort].options.maxOpenedConnections;
}

function reviewConnectionsAndReturn (hostPort, dbName, rmAnyway, result) {

    cache[hostPort].handlers[dbName]--;

    return (function(){
        if ( rmAnyway ) {
            return closeConnection(hostPort, dbName)
        } else {
            if ( !isFreeSlots(hostPort) ) {
                // TODO check frequency
                if ( !cache[hostPort].handlers[dbName] ) return closeConnection(hostPort, dbName);
                else return Q.reject('handlers');
            } else {
                return Q.resolve();
            }
        }
    })()
    .then(
        function(){
            if ( cache[hostPort].queue.length ) {
                var task = cache[hostPort].queue.shift();

                if ( cache[hostPort].connections[task.dbName] ) {
                    //if ( !Q.isPromise(cache[hostPort].connections[task.dbName]) ) {
                    //    console.log('-----');
                    //}
                    task.defer.resolve( cache[hostPort].connections[task.dbName] );
                } else {
                    task.defer.resolve(
                        cache[hostPort].connections[task.dbName] = connect(hostPort, task.dbName)
                    );
                }
            }

            return Q.resolve(result);
        },
        function(error){
            if ( error !== 'handlers' ) {
                return Q.reject(error);
            } else return Q.resolve(result);
        }
    )
}

/**
 * @param {String} hostPort something like this: mongobd://localhost:27017/
 * @param {Object} dbName database name
 *
 * @returns {Promise | Object} mongodb's db
 */
function connect(hostPort, dbName) {
    var D = Q.defer();

    MCL.connect(hostPort + dbName, function (error, db){
        if ( error ) {
            D.reject(error);
        } else {
            D.resolve(db);
        }
    });

    var t = D.promise.then(
        function (db) {
            //console.log('connected to ' + dbName + ' total ' + Object.keys(cache[hostPort].connections).length);
            return Q.resolve(db);
        },
        function (error) {
            //console.log('--- can not connect to ' + dbName);
            //console.log(error);
            return Q.reject(error);
        }
    );

    return t;
    //return Q.nfcall(MCL.connect, hostPort + dbName)
}

function addToQueue (hostPort, dbName) {

    var task = {
        dbName : dbName,
        defer  : Q.defer()
    };

    cache[hostPort].queue.push(task);

    return task.defer.promise;
}

///**
// * @param {String} hostPort something like this: mongobd://localhost:27017/
// */
//function closeFirstConnection(hostPort) {
//    var dbName = Object.keys(cache[hostPort].connections)[0];
//    return closeConnection(hostPort, dbName);
//}

function closeConnection(hostPort, dbName) {
    if (!dbName) return Q.resolve();
    var D = Q.defer();
    cache[hostPort].connections[dbName].then(function(db){
        db.close(true, function (error) {
            //console.log('disconnected ' + dbName + ' total ' + Object.keys(cache[hostPort].connections).length + ( error || ''));
            if (error) return D.reject(error);
            delete cache[hostPort].connections[dbName];
            return D.resolve();
        });
    });
    return D.promise;
}

function clone(p) {
    function C() {
    }

    C.prototype = p;
    return new C;
}

module.exports = Mdbw;
