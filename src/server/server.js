const
    INFO_CL = 'info',
    USER_DB_PREFIX = 'name_surr_notes_user__';

var express      = require('express'),
    jade         = require('jade'),
    fs           = require('fs'),
    path         = require('path'),
    bodyParser   = require('body-parser'),
    cookieParser = require('cookie-parser'),

    PTH_CLNT_FILES         = path.join(__dirname, '..', 'client'),
    API_V1_PREFIX          = '/_/api/v1',
    RGXP_IS_API_V1_PREFIX  = /^\/_\/api\/v1/,

    db   = require('./mdbw')(),
    IdBasedDb = require('./idBasedMongoStorage'),
    user = require('./user')({
        DB_PREFIX : USER_DB_PREFIX,
        INFO_CL   : INFO_CL,
        db        : db
    }),
    auth = require('./auth')({
        cookieName     : 'notes.session',
        loginScreenUrl : '/_/login',
        user           : user,
        sessionStorage : new IdBasedDb({
                database    : 'name_surr_notes_sessions',
                collection  : 'sessions',
                keyField    : 'id',
                db          : db
            }),
        doNotRedirectToLoginScreenIf : [
            RGXP_IS_API_V1_PREFIX
        ]
    }),
    api_v1 = require('./api.v1.js').init({
        DB_PREFIX : USER_DB_PREFIX,
        NOTES_CL  : 'notes',
        TAGS_CL   : 'tags',
        INFO_CL   : INFO_CL,
        db        : db
    }),

    app = express();


app.set('views', path.join(PTH_CLNT_FILES, 'views'));
app.set('view engine', 'jade');


app.use('/_/templates', express.static(path.join(PTH_CLNT_FILES, 'templates')));
app.use('/_/blocks',    express.static(path.join(PTH_CLNT_FILES, 'blocks')));
app.use('/_/js',        express.static(path.join(PTH_CLNT_FILES, 'js')));
app.use('/_/css',       express.static(path.join(PTH_CLNT_FILES, 'css')));
app.use(bodyParser.urlencoded({extended: true, limit:'50mb', parameterLimit:'Infinity'}));
app.use(bodyParser.json({limit:'50mb'}));
app.use(cookieParser());
app.use(auth.sessionGate);

app.use('/old', express.static(path.join(PTH_CLNT_FILES, 'old'))); // TODO move to jade and remove


app.get(  '/_/login', auth.loginScreen);
app.post( '/_/login', auth.login);

app.get(  '/', function (req, res) { res.render('index') });

app.get(     API_V1_PREFIX + '/notes/:id', api_v1.notes.get);
app.delete(  API_V1_PREFIX + '/notes/:id', api_v1.notes.rm);
app.post(    API_V1_PREFIX + '/notes',     api_v1.notes.create);
app.get(     API_V1_PREFIX + '/notes',     api_v1.notes.search);
app.get(     API_V1_PREFIX + '/tags',      api_v1.tags.get);


app.listen(4000, 'localhost');


/*

home or public notes                        notes.surr.name/vova
a note                                      notes.surr.name/vova/note/tG3dF
login URI                                   notes.surr.name/vova/login
search for the user notes                   notes.surr.name/vova/search?tags=node,mongo&scope=local
search for all public notes                 notes.surr.name/vova/search?tags=node,mongo&scope=global
search for all public notes for the group   notes.surr.name/vova/search?tags=node,mongo&scope=group&group=someUsersGroup
search for all public notes for the user    notes.surr.name/vova/search?tags=node,mongo&scope=user&user=charlie

*/
