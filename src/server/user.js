var Q = require('q');


module.exports = Constr;

function Constr ( o ) {
    if ( !(this instanceof Constr) ) return new Constr(o);

    this._DB_PREFIX = o.DB_PREFIX;
    this._CL_NAME   = o.INFO_CL;
    this._db = o.db;
}

Constr.prototype.get = function ( o ) {
    return this._db.get(this._getDbName(o), this._CL_NAME, {login : o.login})
    .then(function(docs){ return docs[0] });
};

Constr.prototype.create = function ( o ) {
    var that = this;
    // TODO check
    return this._db.get(this._getDbName(o), this._CL_NAME, {login : o.login})
    .then(function(user){
        if ( user && user.length ) return Q.reject('user already exists');
        return that._db.put(that._getDbName(o), that._CL_NAME, o, {upsert:false});
    });
};

Constr.prototype.delete = function ( o ) {
    // TODO check
    return this._db.rm(this._getDbName(o), this._CL_NAME, o);
};

Constr.prototype.update = function ( o ) {
    // TODO check
    return this._db.update(this._getDbName(o), this._CL_NAME, o, {multi:false});
};


Constr.prototype._getDbName = function ( o ) {
    return o && o.login
        ? this._DB_PREFIX + o.login
        : ''
};
