/*
 * converts from decimal number system to 62
 *
 * @param {Number} decimal number
 * @returns {String} number in 62 number system
 */
module.exports = convert;

const BASE = 62;
      MAP  = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];

function convert ( decimal ) {

    var invertedSimbols = [],
        integer,
        modulo,
        current = decimal;

    do {
        integer = Math.floor(current/BASE);
        modulo = current%BASE;

        invertedSimbols.push(MAP[modulo]);

        current = integer;

    } while ( integer >= BASE );

    if ( integer ) invertedSimbols.push(MAP[integer]);

    return invertedSimbols.reverse().join('');
}
